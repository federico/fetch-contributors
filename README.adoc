=== Fetch project contributors

This tool fetches project contributors in a format suitable for `debian/copyright` without downloading the whole git commit history.

Example:
```
$ ./list-contributors.py https://github.com/rust-lang/rust
2013-2024 bors <bors@rust-lang.org>                                      33844 bors
2015-2023 Alex Kladov <aleksey.kladov@gmail.com>                          7821 matklad
2015-2024 Ralf Jung <post@ralfj.de>                                       7003 RalfJung
2018-2024 Matthias Krüger <matthias.krueger@famsik.de>                    6911 matthiaskrgr
2015-2024 Guillaume Gomez <guillaume1.gomez@gmail.com>                    5515 GuillaumeGomez
2010-2020 Brian Anderson <andersrb@gmail.com>                             5506 brson
2013-2024 Alex Crichton <alex@alexcrichton.com>                           5082 alexcrichton
2015-2024 Oli Scherer <???>                                               5013 oli-obk
2017-2020 Mazdak Farrokhzad <twingoow@gmail.com>                          4300 Centril
2011-2022 Niko Matsakis <niko@alum.mit.edu>                               4172 nikomatsakis
The listing above was trimmed: 99 authors in total
```

The tool currently supports only github unfortunately. It tries to identify multiple authors, copyright years for each of them, and their email address from the github profile as well as their commits.
It is not specific to Rust projects.
